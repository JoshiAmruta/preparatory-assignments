#include <stdio.h>
#include <stdlib.h>
#include<string.h>
void Add_Number(int a[]);
void Delete_Number(int a[]);
void Max_Number(int a[]);
void Min_Number(int a[]);
void Sum_of_Numbers(int a[]);
//int n=10;

int main()
{
    int choice=0;
    int arr[10]={0};
    do
    {
        
        printf("\n\n0.Exit\n1.Add Number\n2.Delete Number\n3.Find Maximum Number\n4.Find Minimum Number\n5.Sum of Numbers\nEnter Choice:-");
        scanf("%d",&choice);
        switch(choice)
        {
            case 1:Add_Number(arr);
            break;
            case 2:Delete_Number(arr);
            break;
            case 3:Max_Number(arr);
            break;
            case 4:Min_Number(arr);
            break;
            case 5:Sum_of_Numbers(arr);
            break;


        }
    } while (choice!=0);
    
    return 0;
}

void Add_Number(int a[])
{
    int i=0,pos,n;
    printf("Availabale Index list:-");
   for(;i<10;i++)
   {

    if(a[i]==0)
    {
    printf("%d,",i+1);
    }
     
   }
      
   printf("Enter position:-");
   scanf("%d",&pos);
    if(pos>10)
   printf("Insertion is not possible.\n");
   else
   {
    
   printf("Enter number:");
   scanf("%d",&a[pos-1]);
   printf("\nResultant array:");
   for(int j=0;j<10;j++)
   {
    printf("\na[%d]=%d",j+1,a[j]);
   }
   }
}


void Delete_Number(int a[])
{
    int pos,c;
    printf("Availabale Index list with values:-");
   for(int i=0;i<10;i++)

   {

    if(a[i]!=0)
    {
    printf("a[%d]=%d\n",i+1,a[i]);
    }
     
   }

    printf("\n");
    printf("Enter position:-");
    scanf("%d",&pos);
    if(pos>10)
     printf("Deletion is not possible.\n");
    else
    {
        
    //for(c=pos-1;c<n;c++)
       // a[c]=a[c+1];
        //n--;
        a[pos-1]=0;
    printf("Resultant array:\n");
    for(c=0;c<10;c++)
        printf("%d\n",a[c]);
       
    }

}

void Max_Number(int a[])
{
    int i,largest,k=0;
    largest=a[0];
    for(i=1;i<10;i++)
    {
        if(largest<a[i])
        {
        largest=a[i];
        k=i;
        }
    }
printf("\nLargest Element at a[%d] = %d",k+1,a[k]);

}

void Min_Number(int a[])
{
    int i,smallest,k=0;
    smallest=a[0];
    for(i=1;i<10;i++)
    {
        if(smallest>a[i])
        {
        smallest=a[i];
        k=i;
        }
    }
     printf("\nSmallest Element at a[%d] = %d",k+1,a[k]);
}

void Sum_of_Numbers(int a[])
{
    int sum=0;
    for(int i=0;i<10;i++)
    {
    sum+=a[i];
    }
    printf("\nSum of Numbers=%d",sum);
}