#include <stdio.h>
#include<stdlib.h>
#include <string.h>

#define USER_DB		"users.db"
#define ITEM_DB		"items.db"

typedef struct user {
	int id;
	char name[30];
	
}user_t;

typedef struct item {
	int id;
	char name[30];
    int price;
    int quantity;
	
}item_t;

//user function
void user_accept(user_t *u){

    printf("id: ");
    scanf("%d",&u->id);
    printf("name: ");
    scanf("%s",u->name);
    
}

void user_display(user_t *u){

    printf("id :%d name :%s",u->id,u->name);
}

//item function
void item_accept(item_t *i){

    printf("id: ");
    scanf("%d",&i->id);
    printf("name: ");
    scanf("%s",i->name);
    printf("price: ");
	scanf("%lf", &i->price); 
    printf("quantity: ");
	scanf("%lf", i->quantity);
}


void item_display(item_t *i){

    printf("%d, %s, %.2lf, %d\n", i->id, i->name, i->price, i->quantity);
}
void item_add()
{
	item_t i;
	i.id = 0;
	long int size = sizeof(item_t);
	FILE *fptr;
	fptr = fopen("inventory.db","ab+");
	if(fptr == NULL)
	{
		printf("\nFile can not be opened");
		return;
	}
	fseek(fptr,  -size, SEEK_END);
	fread(&i, sizeof(item_t), 1, fptr);
	i.id++;
	printf("\nEnter name of item : ");
	scanf("%s", i.name);
	printf("\nEnter price of item : ");
	scanf("%lf", &i.price);
	printf("\nEnter quantity of item : ");
	scanf("%d", &i.quantity);	
	fseek(fptr, size, SEEK_END);
	fwrite(&i, size, 1, fptr);
	fclose(fptr);	
	return;
}

void item_find_by_name(char name[]) {
	FILE *fp;
	int found = 0;
	item_t i;
	// open the file for reading the data
	fp = fopen(ITEM_DB, "rb");
	if(fp == NULL) {
		perror("failed to open item file");
		return;
	}
	// read all items one by one
	while(fread(&i, sizeof(item_t), 1, fp) > 0) {
		// if book name is matching partially, found 1
		if(strstr(i.name, name) != NULL) {
			found = 1;
			item_display(&i);
		}
	}
	
	fclose(fp);
	if(!found)
		printf("item not found.\n");
}

int get_next_item_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(item_t);
	item_t i;
	
	fp = fopen(ITEM_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file position to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&i, size, 1, fp) > 0)
		// if read is successful get max id
		max = i.id;
	
	fclose(fp);

	return max + 1;
}

void edit_item() 
{

	int id;
	item_t i;
	int found = 0;
	FILE *fp;
	
	printf("enter id: ");
	scanf("%d",&id);
	
	// open items file
	fp = fopen(ITEM_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open items file");
		exit(1);
	}
	// read items one by one and check if user with given id is found.
	while(fread(&i, sizeof(item_t), 1, fp) > 0)
     {
		if( id == i.id) {
			printf("found\n");
			found = 1;
			break;
		}
	}
	 //if found
	if(found) 
	{
		// input new item details from user
		long size = sizeof(item_t);
		item_t np;
		item_accept(&np);
		np.id = i.id;
		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite  details into the file
		fwrite(&i, size, 1, fp);
		printf("profile updated.\n");
		
	}
	else // if not found
		// show message to user that item found.
		printf("items not found.\n");
	// close items file
	fclose(fp);
}

void delete_item()
{
    int id, found=0;
    item_t i;
    printf("Enter product id: "); 
    scanf("%d",&id);    

    FILE *fp;
    FILE *fp_tmp;
    fp=fopen(USER_DB,"rb");
    fp_tmp=fopen(ITEM_DB,"wb");         //create item database for deleting the product detail

     if(fp==NULL)
    {
        perror("file not found");
        return;
    }

     if(fp_tmp==NULL)
    {
        printf("file not found");
        return;
    }

    while(fread(&i,sizeof(item_t),1,fp)>0)
    {
        
        if(id==i.id){
            found = 1;
            printf("item %d is deleted from item file",i.id);
              
        }
        else
        {
            fwrite(&i, sizeof(item_t),1,fp_tmp);
        }
        
    }
    if(!found)
    {
        printf(" item not found \n");

    }
            
        
    fclose(fp);
    fclose(fp_tmp);

    remove(USER_DB);
    rename(ITEM_DB,ITEM_DB);
}

void display_all()
{
	FILE *fp;
	user_t u;
	long size=sizeof(user_t);
	fp=fopen(USER_DB,"rb");
	if (fp==NULL)
	{
		perror("\nFailed to open users database");
		return;
	}

	while(fread(&u,size,1,fp)>0)
	{
		user_display(&u);
	}

	fclose(fp);
}


//user function
void user_accept(user_t *u);
void user_display(user_t *u);

//item function
void item_accept(item_t *i);
void item_display(item_t *i);


int main(){
    

    int choice;
    char name[30];
	do {
		printf("\n");
		printf("\n1. Add Item\n2. Find Item\n3. Edit Item\n4. Delete Item\n5. Display All");
		printf("\nEnter your choice : ");
		scanf("%d", &choice);
		switch(choice) {
			
			case 1: // Add Item
                    item_add();
				break;
			case 2://Find Item
                printf("Enter Item name:");
				scanf("%s",name);
				item_find_by_name(name);
				break;
			case 3://Edit Item
                edit_item();
                break;
			case 4: //Delete Item
                delete_item();
			    break;	
                
			case 5: //Display All
				display_all();
				break;
        }
    }while (choice != 0);	
	return 0;
}